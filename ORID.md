### Objective
今天上午进行了code review，学习了SQL Basic，包括DDL和DML等,今天下午学习了Spring Data JPA   
并进行了相关的案例测试，最后学习了开发环境和测试环境的分离。其中，我印象最深刻的是

### Reflective
relaxed

### Interpretive
今天的学习内容比昨天的简单，作业也比昨天的少一点，学习过程中没有碰到太让人困扰的问题，整体的  
学习比昨天轻松很多。

### Decisional
今天是我第一次接触JPA，以前只使用过Mybatis，通过今天的学习，感觉JPA的使用更加自动、快捷，非常  
方便。在今后的学习中我也会经常练习，争取早日熟练掌握。