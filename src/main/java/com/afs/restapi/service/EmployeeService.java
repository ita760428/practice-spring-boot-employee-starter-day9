package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private final EmployeeRepository employeeRepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository,EmployeeRepository employeeRepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.employeeRepository = employeeRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }


    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = findById(id);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        employeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeRepository.findAllByGender(gender);
    }

    public Employee create(Employee employee) {
        return employeeRepository.save(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeRepository.findAll(PageRequest.of(page-1,size)).toList();
    }

    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }
}
